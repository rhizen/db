package db

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/pkg/errors"
)

func Open(dsn string) (*sql.DB, error) {
	database, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("connect to %s fail", dsn))
	}

	database.SetMaxOpenConns(32)
	database.SetMaxIdleConns(2)
	database.SetConnMaxLifetime(300 * time.Second)

	err = database.Ping()
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("ping to %s fail", dsn))
	}
	return database, err
}

