package db

import (
  "database/sql"

  sq "github.com/Masterminds/squirrel"
  "github.com/Masterminds/structable"
)

type Querier struct {
  db    *sql.DB
  cache sq.DBProxyBeginner
}

type Table interface {
  TableName() string
}

func NewQuerier(db *sql.DB) *Querier {
  cache := sq.NewStmtCacheProxy(db)
  //cachedb := sq.StatementBuilder.RunWith(cache)
  return &Querier{db, cache}
}

func (qr *Querier) Query(query string, args interface{}) (*sql.Rows, error) {
  return qr.db.Query(query, args)
}

func (qr *Querier) CreateSchema(sql string) error {
  r, err := qr.db.Exec(sql)

  if err != nil {
    return err
  }

  _, err = r.RowsAffected()
  return err
}

func (qr *Querier) Insert(tbl Table, data map[string]interface{}) (int64, error) {
  columns, values := kvpairs(data)
  query := sq.Insert(tbl.TableName()).
    Columns(columns...).
    Values(values...).
    RunWith(qr.cache)

  rs, err := query.Exec()
  if err != nil {
    return 0, err
  }

  id, err := rs.LastInsertId()
  if err != nil {
    return 0, err
  }

  return id, err
}

func (qr *Querier) Find(tbl Table, in *[]structable.Recorder, where structable.WhereFunc) error {
  t := structable.New(qr.cache, "mysql").Bind(tbl.TableName(), tbl)
  var err error
  *in, err = structable.ListWhere(t, where)
  return err
}

func (qr *Querier) Update(tbl Table, id int64, data map[string]interface{}) error {
  query := sq.Update(tbl.TableName()).Where("id = ?", id).SetMap(data)

  _, err := query.Exec()
  return err
}

func kvpairs(data map[string]interface{}) ([]string, []interface{}) {
  cols := make([]string, 0)
  vals := make([]interface{}, 0)
  for c, v := range data {
    cols = append(cols, c)
    vals = append(vals, v)
  }

  return cols, vals
}
